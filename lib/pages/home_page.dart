import 'package:flutter/material.dart';
import 'package:harishportfolio/config/assets.dart';
import 'package:harishportfolio/tabs/about_tab.dart';
import 'package:harishportfolio/tabs/blog_tab.dart';
import 'package:harishportfolio/tabs/projects_tab.dart';
import 'package:harishportfolio/widgets/theme_inherited_widget.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;
  static List<Widget> tabWidgets = <Widget>[
    AboutTab(),
    //BlogTab(),
    ProjectsTab(),
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: ThemeSwitcher.of(context).isDarkModeOn
                ? Image.asset(
                    Assets.light,
                    height: 20,
                    width: 20,
                  )
                : Image.asset(
                    Assets.moon,
                    height: 20,
                    width: 20,
                  ),
            onPressed: () => ThemeSwitcher.of(context).switchDarkMode(),
          )
        ],
      ),
      body: Center(
        child: tabWidgets.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: 'About',
          ),
          /*BottomNavigationBarItem(
            icon: Icon(Icons.chrome_reader_mode),
            label: 'Blog',
          ),*/
          BottomNavigationBarItem(
            icon: Icon(Icons.chrome_reader_mode_sharp),
            label: 'Projects',
          )
        ],
        currentIndex: _selectedIndex,
        onTap: (index) => setState(() => _selectedIndex = index),
        selectedItemColor: Theme.of(context).accentColor,
      ),
    );
  }
}

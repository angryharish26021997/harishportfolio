import 'package:flutter/material.dart';

class Project {
  String image;
  String name;
  String description;
  String link;
  String language;

  Project(
      {@required this.image,
      @required this.name,
      @required this.description,
      this.link,
      @required this.language});
}

import 'dart:html' as html;

import 'package:flutter/material.dart';
import 'package:harishportfolio/models/project_model.dart';

class ProjectWidget extends StatelessWidget {
  final Project _project;
  final double _bottomPadding;

  ProjectWidget(this._project, this._bottomPadding);
  String firstHalf;
  String secondHalf;

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;

    if (_project.description.length > 100) {
      firstHalf = _project.description.substring(0, 100);

    } else {
      firstHalf = _project.description;
      secondHalf = "";
    }
    return Card(
      margin: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, _bottomPadding),
      child: InkWell(
        onTap: () {
          onProjectClick();
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Expanded(
                  flex: 40,
                  child: Image.asset(

                    _project.image,

                    color: _project.name == "Corsalite" ? Colors.green.shade400:null,
                    width: width * .25,
                    height: width * .25,
                  )),
              Expanded(
                flex: 3,
                child: Container(),
              ),
              Expanded(
                flex: 60,
                child: Container(
                  padding: EdgeInsets.only(top: 8.0),
                  child: Wrap(
                    direction: Axis.horizontal,
                    children: <Widget>[

                      Text(
                        _project.name,
                        //style: Theme.of(context).textTheme.headline6,
                        textScaleFactor: 1.5,
                      ),
                      SizedBox(
                        height: height * .01,
                      ),
                      Text(
                        _project.language,
                        textScaleFactor: 0.6,
                        style: Theme.of(context).textTheme.caption,
                      ),
                      SizedBox(
                        height: height * .01,
                      ),
                      Text(
                        (firstHalf + "...") ,
                        textScaleFactor: 1,
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void onProjectClick() {
    if (_project.link != null)
      html.window.open(_project.link, 'Harish Sivachalam');
  }
}

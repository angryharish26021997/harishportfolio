class Assets {
  static const moon = 'assets/moon.png';
  static const light = 'assets/light.png';
  static const avatar = 'assets/avatar.jpg';
  static const facebook = 'assets/facebook.png';
  static const github = 'assets/github.png';
  static const instagram = 'assets/instagram.png';
  static const linkedin = 'assets/linkedin.png';
  static const medium = 'assets/medium.png';
  static const medium_light = 'assets/medium_light.png';
  static const twitter = 'assets/twitter.png';
  static const gitlab = 'assets/gitlab.png';
  static const trivz = 'assets/works/trivz.png';
  static const bui = 'assets/aagnia.png';
  static const practically = 'assets/works/practically.jpg';
  static const teta = 'assets/works/teta.jpg';
  static const corsalite = 'assets/works/corsalite.png';
  static const dura = 'assets/works/dura-logo.png';
  static const myhubplus = 'assets/myhubplus.png';
}

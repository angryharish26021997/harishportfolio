import 'package:harishportfolio/models/project_model.dart';

import 'assets.dart';

final List<Project> projects = [
  Project(
      name: 'TETA',
      image: Assets.teta,
      language: "Java Android",
      description:
          'Teta was a Tamilnadu Electronics Technician’s Association.\nThis is like a social media app. Which was communicating to the TamilNadu Technician’s to share the latest things and clearfy the doubt each others. ',
      link:
          'https://play.google.com/store/apps/details?id=com.newareeliya.teta'),
  Project(
      name: 'TETA Sales & Service',
      image: Assets.teta,
      language: "Java Android",
      description:
          'Teta sales and service app was the one of the ecom and service shop details app.In this app we can buy the electronic oriented product like ic – circuit and chips etc.., And the technician also find the nearest place of technician shop or their friends shop. In this app contains a 2 module package like ecom and service app contains a single app',
      link:
          'https://play.google.com/store/apps/details?id=com.newareeliya.ecom'),
  Project(
      name: 'Corsalite',
      image: Assets.corsalite,
      language: "Java Android",
      description:
          'Corsalite is an experiential learning app for kids which revolutionizes the way to learn math & science concepts. And this is the Android Native Program it was similar to Practically student App. But The Practically and corsalite app difference is corsalite which was contains only Test and Class wised Video and Assignment Topics(All type of test like schedule and mock etc..,).',
      link: ''),
  Project(
      name: 'Dura Rock',
      image: Assets.dura,
      language: "Flutter",
      description:
          'E-Commerce based application and order the items and tracking the delivery items',
      link: 'https://play.google.com/store/apps/details?id=com.renso.durarock'),
  Project(
      name: 'Teacher Learning App',
      language: "Flutter",
      image: Assets.practically,
      description:
          'This Application is another of Pratically app, Mainly create the assignments, Review the Assignments and contacting the live class also.',
      link:
          'https://play.google.com/store/apps/details?id=com.thirdflix.practically'),
  Project(
      name: 'Practically Learning App',
      language: "Flutter",
      image: Assets.practically,
      description:
          '"Practically" is an experiential learning app for kids which revolutionizes the way tolearn math & science concepts. Practically was contains Simulation and AR Type Videos and Normal Videos and Test',
      link:
          'https://play.google.com/store/apps/details?id=com.thirdflix.practically'),
  Project(
    name: 'Trivia',
    language: "React-native",
    image: Assets.trivz,
    description:
        'Trivia is a gk App which is one part of coralite App. Trivia is a cross-platform App',
  ),
  Project(
      name: 'BUI Component',
      language: "Flutter",
      image: Assets.bui,
      description:
          'This is the package of crm product. Which was contains like screen and basic logic. Like Dashboard design tab design json form design and otp , login, forgot password all designs contains.',
      link: 'https://pub.dev/packages/bui_component'),
  Project(
      name: 'BUI',
      language: "Flutter",
      image: Assets.bui,
      description:
          'This is a CRM App. Which is implement is BUI Component. In this app Contains all business logic',
      link: 'https://pub.dev/packages/bui_component'),
  Project(
      name: 'MyHubPlus',
      language: "Java",
      image: Assets.myhubplus,
      description:
          'This application which was used to warehouse operation. Use to inward, outward, Pick and warehouse oriented operations.',
      link: 'https://play.google.com/store/apps/details?id=com.cube.myhubplus'),
];

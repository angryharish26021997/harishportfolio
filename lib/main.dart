import 'package:flutter/material.dart';
import 'package:harishportfolio/pages/home_page.dart';
import 'package:harishportfolio/widgets/theme_inherited_widget.dart';
import 'package:url_strategy/url_strategy.dart';

import 'config/themes.dart';

void main() {
  setPathUrlStrategy();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ThemeSwitcherWidget(
      initialDarkModeOn: false,
      child: Harishdroid(),
    );
  }
}

class Harishdroid extends StatelessWidget {
  const Harishdroid({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Harish Sivachalam',
      theme: ThemeSwitcher.of(context).isDarkModeOn
          ? darkTheme(context)
          : lightTheme(context),
      home: HomePage(),
    );
  }
}
